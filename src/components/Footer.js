import React from 'react';
import "./Footer.css";


class Footer extends React.Component {


    render() {
        return (
            <footer>
                <div id="Footer" className="footer">
                    <div className="container">
                        <div className="footerlogo">
                            <div className="wimma">
                                <a href="https://www.wimmalab.org/" className="page-scroll">
                                    <img src="https://www.wimmalab.org/img/wimmalablogo-01.png" alt="Wimma Lab" />
                                </a>
                            </div>
                            <div className="jamk">
                                <a href="https://jamk.fi/Etusivu">
                                    <img src="https://www.jamk.fi/globalassets/yhteiset-lohkot-ja-tiedostot--global-blocks-and-files/muut-logot-ja-laatuleimat/jamk_tunnus_valkoinen_nimella_suomi.png" alt="Jamk" />
                                </a>
                            </div>
                        </div>


                    </div>


                </div>
            </footer>
        )
    }
}


export default Footer;