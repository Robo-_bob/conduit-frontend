import React from 'react';
import Footer from './Footer';


class Privacy extends React.Component {
    render() {
        return (
            <div className="privacy">

                <div className="container page">
                    <div className="row">
                        <div className="col-xs-12">
                            <div>
                                <h1>Privacy Policy</h1>




                                <p>Rekisteri- ja tietosuojaseloste<br />T&auml;m&auml; on Yrityksen henkil&ouml;tietolain (10 ja 24 &sect;) ja EU:n yleisen tietosuoja-asetuksen (GDPR)<br />mukainen rekisteri- ja tietosuojaseloste. Laadittu 26.10.2020. Viimeisin muutos 26.10.2020.</p>
                                <p>1. Rekisterinpit&auml;j&auml;<br />Wimma Lab<br />Campus Dynamo<br />Piippukatu 2<br />40100 Jyv&auml;skyl&auml;<br />Finland</p>
                                <p>2. Rekisterist&auml; vastaava yhteyshenkil&ouml;<br />AA6135@jamk.fi</p>
                                <p>3. Rekisterin nimi<br />Verkkopalvelun k&auml;ytt&auml;j&auml;rekisteri</p>
                                <p>4. Oikeusperuste ja henkil&ouml;tietojen k&auml;sittelyn tarkoitus<br />EU:n yleisen tietosuoja-asetuksen mukainen oikeusperuste henkil&ouml;tietojen k&auml;sittelylle on<br />- henkil&ouml;n suostumus (dokumentoitu, vapaaehtoinen, yksil&ouml;ity, tietoinen ja yksiselitteinen)<br />- sopimus, jossa rekister&ouml;ity on osapuolena<br />- laki (mik&auml;)<br />- julkisen teht&auml;v&auml;n hoitaminen (mihin perustuu), tai<br />- rekisterinpit&auml;j&auml;n oikeutettu etu (esim. asiakassuhde, ty&ouml;suhde, j&auml;senyys).</p>
                                <p>Henkil&ouml;tietojen k&auml;sittelyn tarkoitus on yhteydenpito asiakkaisiin, asiakassuhteen yll&auml;pito,<br />markkinointi tms.</p>
                                <p>Tietoja ei k&auml;ytet&auml; automatisoituun p&auml;&auml;t&ouml;ksentekoon tai profilointiin.</p>
                                <p>5. Rekisterin tietosis&auml;lt&ouml;<br />Rekisteriin tallennettavia tietoja ovat: henkil&ouml;n nimi, asema, yritys/organisaatio, yhteystiedot<br />(puhelinnumero, s&auml;hk&ouml;postiosoite, osoite), www-sivustojen osoitteet, verkkoyhteyden IP-osoite,<br />tunnukset/profiilit sosiaalisen median palveluissa, tiedot tilatuista palveluista ja niiden muutoksista,<br />laskutustiedot, muut asiakassuhteeseen ja tilattuihin palveluihin liittyv&auml;t tiedot.</p>
                                <p>Kerro t&auml;ss&auml; my&ouml;s tietojen s&auml;ilytysaika, mik&auml;li mahdollista. Kerro my&ouml;s, jos tiedot esimerkiksi<br />anonymisoidaan tietyn ajan kuluttua.</p>
                                <p>6. S&auml;&auml;nn&ouml;nmukaiset tietol&auml;hteet<br />Rekisteriin tallennettavat tiedot saadaan asiakkaalta mm. www-lomakkeilla l&auml;hetetyist&auml; viesteist&auml;,<br />s&auml;hk&ouml;postitse, puhelimitse, sosiaalisen median palvelujen kautta, sopimuksista, asiakastapaamisista<br />ja muista tilanteista, joissa asiakas luovuttaa tietojaan.</p>
                                <p>7. Tietojen s&auml;&auml;nn&ouml;nmukaiset luovutukset ja tietojen siirto EU:n tai ETA:n ulkopuolelle<br />Tietoja ei luovuteta s&auml;&auml;nn&ouml;nmukaisesti muille tahoille. Tietoja voidaan julkaista silt&auml; osin kuin niin<br />on sovittu asiakkaan kanssa.</p>
                                <p>Tietoja voidaan siirt&auml;&auml; rekisterinpit&auml;j&auml;n toimesta my&ouml;s EU:n tai ETA:n ulkopuolelle.</p>
                                <p>Mik&auml;li luovutat henkil&ouml;tietoja eri tahoille, kerro t&auml;ss&auml; mahdolliset vastaanottajat tai<br />vastaanottajaryhm&auml;t.</p>
                                <p>8. Rekisterin suojauksen periaatteet<br />Rekisterin k&auml;sittelyss&auml; noudatetaan huolellisuutta ja tietoj&auml;rjestelmien avulla k&auml;sitelt&auml;v&auml;t tiedot<br />suojataan asianmukaisesti. Kun rekisteritietoja s&auml;ilytet&auml;&auml;n Internet-palvelimilla, niiden laitteiston<br />fyysisest&auml; ja digitaalisesta tietoturvasta huolehditaan asiaankuuluvasti. Rekisterinpit&auml;j&auml; huolehtii<br />siit&auml;, ett&auml; tallennettuja tietoja sek&auml; palvelimien k&auml;ytt&ouml;oikeuksia ja muita henkil&ouml;tietojen<br />turvallisuuden kannalta kriittisi&auml; tietoja k&auml;sitell&auml;&auml;n luottamuksellisesti ja vain niiden ty&ouml;ntekij&ouml;iden<br />toimesta, joiden ty&ouml;nkuvaan se kuuluu.</p>
                                <p>9. Tarkastusoikeus ja oikeus vaatia tiedon korjaamista<br />Jokaisella rekisteriss&auml; olevalla henkil&ouml;ll&auml; on oikeus tarkistaa rekisteriin tallennetut tietonsa ja vaatia<br />mahdollisen virheellisen tiedon korjaamista tai puutteellisen tiedon t&auml;ydent&auml;mist&auml;. Mik&auml;li henkil&ouml;<br />haluaa tarkistaa h&auml;nest&auml; tallennetut tiedot tai vaatia niihin oikaisua, pyynt&ouml; tulee l&auml;hett&auml;&auml; kirjallisesti<br />rekisterinpit&auml;j&auml;lle. Rekisterinpit&auml;j&auml; voi pyyt&auml;&auml; tarvittaessa pyynn&ouml;n esitt&auml;j&auml;&auml; todistamaan<br />henkil&ouml;llisyytens&auml;. Rekisterinpit&auml;j&auml; vastaa asiakkaalle EU:n tietosuoja-asetuksessa s&auml;&auml;detyss&auml; ajassa<br />(p&auml;&auml;s&auml;&auml;nt&ouml;isesti kuukauden kuluessa).</p>
                                <p>10. Muut henkil&ouml;tietojen k&auml;sittelyyn liittyv&auml;t oikeudet<br />Rekisteriss&auml; olevalla henkil&ouml;ll&auml; on oikeus pyyt&auml;&auml; h&auml;nt&auml; koskevien henkil&ouml;tietojen poistamiseen<br />rekisterist&auml; ("oikeus tulla unohdetuksi"). Niin ik&auml;&auml;n rekister&ouml;idyill&auml; on muut EU:n yleisen<br />tietosuoja-asetuksen mukaiset oikeudet kuten henkil&ouml;tietojen k&auml;sittelyn rajoittaminen tietyiss&auml;<br />tilanteissa. Pyynn&ouml;t tulee l&auml;hett&auml;&auml; kirjallisesti rekisterinpit&auml;j&auml;lle. Rekisterinpit&auml;j&auml; voi pyyt&auml;&auml;<br />tarvittaessa pyynn&ouml;n esitt&auml;j&auml;&auml; todistamaan henkil&ouml;llisyytens&auml;. Rekisterinpit&auml;j&auml; vastaa asiakkaalle<br />EU:n tietosuoja-asetuksessa s&auml;&auml;detyss&auml; ajassa (p&auml;&auml;s&auml;&auml;nt&ouml;isesti kuukauden kuluessa).</p>



                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Privacy;
