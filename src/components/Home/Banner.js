import React from 'react';

const Banner = ({ appName, token }) => {
  if (token) {
    return null;
  }
  return (
    <div id="Banner" className="banner">
      <div className="container">
      </div>
    </div>
  );
};

export default Banner;
