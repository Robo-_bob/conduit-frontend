import Banner from './Banner';
import MainView from './MainView';
import Footer from '../Footer';
import React from 'react';
import Tags from './Tags';
import agent from '../../agent';
import { connect } from 'react-redux';
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  APPLY_TAG_FILTER
} from '../../constants/actionTypes';

import { Link } from 'react-router-dom';
import CookieConsent from "react-cookie-consent";


const Promise = global.Promise;

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onClickTag: (tag, pager, payload) =>
    dispatch({ type: APPLY_TAG_FILTER, tag, pager, payload }),
  onLoad: (tab, pager, payload) =>
    dispatch({ type: HOME_PAGE_LOADED, tab, pager, payload }),
  onUnload: () =>
    dispatch({ type: HOME_PAGE_UNLOADED })
});

class Home extends React.Component {
  componentWillMount() {
    const tab = this.props.token ? 'feed' : 'all';
    const articlesPromise = this.props.token ?
      agent.Articles.feed :
      agent.Articles.all;

    this.props.onLoad(tab, articlesPromise, Promise.all([agent.Tags.getAll(), articlesPromise()]));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">

        <Banner token={this.props.token} appName={this.props.appName} />

        <div className="container page">
          <div className="row">
            <MainView />

            <div className="col-md-3">
              <div className="sidebar">

                <p>Popular Tags</p>

                <Tags
                  tags={this.props.tags}
                  onClickTag={this.props.onClickTag} />

              </div>
            </div>
          </div>
        </div>


        <CookieConsent
          location="bottom"
          buttonText="I agree"
          cookieName="cookies"
          style={{ background: "#074a61" }}
          buttonStyle={{ color: "#4e503b", fontSize: "16px" }}
          expires={150}
          debug="false"
        >
          These cookies make it possible to improve the comfort and performance of websites so as to make various functions available. This website privacy policy complies with Europian Union data protection legislation, including GDPR.{"  "}

          <span style={{ fontSize: "12px" }}>
            <Link className="privacy" to={`/privacy`}>
              Read Privacy Policy.
          </Link>
          </span>
        </CookieConsent>



        <Footer />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
