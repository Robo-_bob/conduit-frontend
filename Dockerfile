FROM node:12

ARG api_root
ENV REACT_APP_API_ROOT=$api_root

RUN useradd nodeuser -m -d /usr/src/app
USER nodeuser

# Allow nodeuser to run npm install globally
ENV NPM_CONFIG_PREFIX=/usr/src/app/.npm-global

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json ./

# Bundle app source
COPY . .

# Install tool to report vulnerabilities in html format
RUN npm install -P npm-audit-html

RUN npm install
# If you are building your code for production
RUN npm ci --only=production

# Create folder for audit reports
RUN mkdir reports
# Check vulnerabilities, fix, and check again and save result into html file
RUN sh audit-json-html.sh reports/audit-frontend-prefix
RUN npm audit fix
RUN sh audit-json-html.sh reports/audit-frontend-postfix

EXPOSE 4100
CMD [ "npm", "start" ]