#!/bin/bash

NOW=$(date '+%Y-%m-%d_%H.%M.%S')

FILE="$1"
if [ -z "$FILE" ]
then
  FILE="audit-frontend-report-$NOW"
fi
echo "Run audit to file $FILE{json|html}"

npm audit --json > $FILE.json
npm audit --json | node_modules/npm-audit-html/index.js --output $FILE.html
